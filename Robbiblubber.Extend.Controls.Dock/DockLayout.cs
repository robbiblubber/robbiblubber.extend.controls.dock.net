﻿using System;

using Microsoft.Win32;

using WeifenLuo.WinFormsUI.Docking;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Extend.Controls.Dock
{
    /// <summary>This class provides application layout functionality for DockPanel applications.</summary>
    public static class DockLayout
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the dock panel layout.</summary>
        /// <param name="dock">Dock content.</param>
        /// <param name="name">Form name.</param>
        public static void SaveDockLayout(this DockPanel dock, string name = null)
        {
            if(LayoutOp.FileOrKey == null) LayoutOp.Initialize();

            if(name == null) { name = dock.Parent.Name + ".Dock"; }

            if(LayoutOp.UseRegistry)
            {
                Registry.SetValue(LayoutOp.FileOrKey + '\\' + name, "top", dock.DockTopPortion);
                Registry.SetValue(LayoutOp.FileOrKey + '\\' + name, "bottom", dock.DockBottomPortion);
                Registry.SetValue(LayoutOp.FileOrKey + '\\' + name, "left", dock.DockLeftPortion);
                Registry.SetValue(LayoutOp.FileOrKey + '\\' + name, "right", dock.DockRightPortion);
            }
            else
            {
                Ddp cfg;
                try
                {
                    cfg = Ddp.Load(LayoutOp.FileOrKey);
                }
                catch(Exception) { cfg = new Ddp(); }

                cfg.SetDouble(name + "/top", dock.DockTopPortion);
                cfg.SetDouble(name + "/bottom", dock.DockBottomPortion);
                cfg.SetDouble(name + "/left", dock.DockLeftPortion);
                cfg.SetDouble(name + "/right", dock.DockRightPortion);

                cfg.Save(LayoutOp.FileOrKey);
            }
        }


        /// <summary>Restore the dock content layout.</summary>
        /// <param name="dock">Form.</param>
        /// <param name="name">Form name.</param>
        public static void RestoreDockLayout(this DockPanel dock, string name = null)
        {
            if(LayoutOp.FileOrKey == null) LayoutOp.Initialize();

            if(name == null) { name = dock.Parent.Name + ".Dock"; }
            if(LayoutOp.UseRegistry)
            {
                dock.DockTopPortion = (double) Registry.GetValue(LayoutOp.FileOrKey + '\\' + name, "top", dock.DockTopPortion);
                dock.DockBottomPortion = (double) Registry.GetValue(LayoutOp.FileOrKey + '\\' + name, "bottom", dock.DockBottomPortion);
                dock.DockLeftPortion = (double) Registry.GetValue(LayoutOp.FileOrKey + '\\' + name, "left", dock.DockLeftPortion);
                dock.DockRightPortion = (double) Registry.GetValue(LayoutOp.FileOrKey + '\\' + name, "right", dock.DockRightPortion);
            }
            else
            {
                try
                {
                    Ddp cfg = Ddp.Load(LayoutOp.FileOrKey);

                    dock.DockTopPortion = cfg.GetDouble(name + "/top", dock.DockTopPortion);
                    dock.DockBottomPortion = cfg.GetDouble(name + "/bottom", dock.DockBottomPortion);
                    dock.DockLeftPortion = cfg.GetDouble(name + "/left", dock.DockLeftPortion);
                    dock.DockRightPortion = cfg.GetDouble(name + "/right", dock.DockRightPortion);
                }
                catch(Exception) {}
            }
        }


        /// <summary>Saves the dock content state.</summary>
        /// <param name="form">Dock content.</param>
        /// <param name="name">Form name.</param>
        public static void SaveState(this DockContent form, string name = null)
        {
            if(LayoutOp.FileOrKey == null) LayoutOp.Initialize();
            if((form.DockState == DockState.Unknown) || (form.DockState == DockState.Hidden)) return;

            if(name == null) { name = form.Name; }

            if(LayoutOp.UseRegistry)
            {
                Registry.SetValue(LayoutOp.FileOrKey + '\\' + name, "dock", (int) form.DockState);
            }
            else
            {
                Ddp cfg;
                try
                {
                    cfg = Ddp.Load(LayoutOp.FileOrKey);
                }
                catch(Exception) { cfg = new Ddp(); }

                cfg.SetInteger(name + "/dock", (int) form.DockState);
                cfg.Save(LayoutOp.FileOrKey);
            }
        }


        /// <summary>Shows the docking content on its last location.</summary>
        /// <param name="form">Dock content.</param>
        /// <param name="panel">Dock panel.</param>
        /// <param name="defaultState">Default state.</param>
        /// <param name="name">Form name.</param>
        public static void Show2(this DockContent form, DockPanel panel, DockState defaultState, string name = null)
        {
            if(LayoutOp.FileOrKey == null) LayoutOp.Initialize();

            DockState state = defaultState;
            if(name == null) { name = form.Name; }
            if(LayoutOp.UseRegistry)
            {
                state = (DockState) Registry.GetValue(LayoutOp.FileOrKey + '\\' + name, "dock", defaultState);
            }
            else
            {
                try
                {
                    Ddp cfg = Ddp.Load(LayoutOp.FileOrKey);

                    state = (DockState) cfg.GetInteger(name + "/dock", (int) defaultState);
                }
                catch(Exception) {}
            }

            form.Show(panel, state);
        }
    }
}
